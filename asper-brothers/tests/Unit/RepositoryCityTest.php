<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\CityRepository;
use App\City;

class RepositoryCityTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIfWillGetAllCities()
    {
        City::create([
            'name' => 'Warszawa'
        ]);

        City::create([
            'name' => 'Kraków'
        ]);

        City::create([
            'name' => 'Łódź'
        ]);

        $repo = new CityRepository();

        $this->assertCount(3, $repo->getAll());
    }

    public function testIfCityWithGivenNameAlreadyExists() {
        
        City::create([
            'name' => 'Karków'
        ]);

        $repo = new CityRepository();

        $this->assertNotNull($repo->getIfExists('Karków'));
    }
}
