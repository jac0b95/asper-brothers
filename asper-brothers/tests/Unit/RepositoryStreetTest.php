<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\City;
use App\Street;
use App\Repositories\StreetRepository;

class RepositoryStreetTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIfStreetWithGivenNameAlreadyExists()
    {
        $city = City::create([ 'name' => 'Łódź' ]);

        Street::create([
            'name' => 'Mazowiecka',
            'city_id' => $city->id
        ]);

        $repo = new StreetRepository();

        $this->assertNotNull($repo->getIfExists('Mazowiecka', $city->id));
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIfWillReturnStreetAssociatedToCity()
    {
        $city = City::create([ 'name' => 'Warszawa' ]);
        
        $second_city = City::create([ 'name' => 'Łódź' ]);

        Street::create([
            'name' => 'Marszałkowska',
            'city_id' => $city->id
        ]);
        
        Street::create([
            'name' => 'Mazowiecka',
            'city_id' => $second_city->id
        ]);

        Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        $repo = new StreetRepository();

        $this->assertCount(2, $repo->getByProperties($city->id));
    }
}
