<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\ObjectRepository;
use App\City;
use App\Street;
use App\Year;
use App\Object;

class RepositoryObjectTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIfObjectAssociatedYearStreetAndCityExists()
    {
        $city = City::create([
            'name' => 'Łódź'
        ]);

        $street = Street::create([
            'name' => 'Mazowiecka',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2019',
            'street_id' => $street->id
        ]);

        $object = Object::create([
            'name' => 'Plac',
            'year_id' => $year->id
        ]);


        $repo = new ObjectRepository();
        $object = $repo->getIfExists('Plac', $city, $street, $year);

        $this->assertNotNull($object);
    }

    public function testIfWillReturnRecordsByGivenCityStreetAndYearAsStrings()
    {
        $city = City::create([
            'name' => 'Łódź'
        ]);

        $street = Street::create([
            'name' => 'Mazowiecka',
            'city_id' => $city->id
        ]);

        $year = Year::create([
            'year' => '2019',
            'street_id' => $street->id
        ]);

        $second_year = Year::create([
            'year' => '2018',
            'street_id' => $street->id
        ]);

        $first_object = Object::create([
            'name' => 'Plac',
            'year_id' => $year->id
        ]);

        $second_object = Object::create([
            'name' => 'Plac 2',
            'year_id' => $year->id
        ]);

        $third_object = Object::create([
            'name' => 'Plac 3',
            'year_id' => $second_year->id
        ]);

        $fourth_object = Object::create([
            'name' => 'Plac 4',
            'year_id' => $year->id
        ]);

        $repo = new ObjectRepository();

        $properties = [
            'city' => $city->name,
            'street' => $street->name,
            'year' => $year->year
        ];

        $this->assertCount(3, $repo->getByProperties($properties));
    }
}
