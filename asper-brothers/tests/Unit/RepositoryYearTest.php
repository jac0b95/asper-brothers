<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\City;
use App\Street;
use App\Year;
use App\Repositories\YearRepository;
use App\Repositories\YearRepositoryInterface;

class RepositoryYearTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIfStreetWithGivenNameAlreadyExists()
    {
        $city = City::create([ 'name' => 'Łódź' ]);

        $street = Street::create([
            'name' => 'Mazowiecka',
            'city_id' => $city->id
        ]);

        Year::create([
            'year' => '2019',
            'street_id' => $street->id
        ]);

        $repo = new YearRepository();

        $properties = array(
            'city' => $city->id,
            'street' => $street->id
        );

        $this->assertNotNull($repo->getIfExists('2019', $properties));
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIfWillReturnYearAssociatedToCityAndStreet()
    {
        $city = City::create([ 'name' => 'Warszawa' ]);
        
        $second_city = City::create([ 'name' => 'Łódź' ]);

       $street = Street::create([
            'name' => 'Marszałkowska',
            'city_id' => $city->id
        ]);
        
       $second_street = Street::create([
            'name' => 'Mazowiecka',
            'city_id' => $second_city->id
        ]);

       $third_street = Street::create([
            'name' => 'Jana Pankiewicza',
            'city_id' => $city->id
        ]);

        Year::create([
            'year' => '2019',
            'street_id' => $street->id
        ]);

        Year::create([
            'year' => '2017',
            'street_id' => $third_street->id
        ]);

        Year::create([
            'year' => '2016',
            'street_id' => $third_street->id
        ]);
        Year::create([
            'year' => '2015',
            'street_id' => $second_street->id
        ]);
       
        $repo = new YearRepository();

        $properties = array(
            'city' => $city->id,
            'street' => $third_street->id
        );

        $this->assertCount(2, $repo->getByProperties($properties));
    }
}
