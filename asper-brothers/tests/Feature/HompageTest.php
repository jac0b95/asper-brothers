<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HompageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIfHomagepageLoads()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
