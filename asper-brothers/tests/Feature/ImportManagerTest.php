<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use App\Object;

class ImportManagerTest extends TestCase
{
    use RefreshDatabase;

    public function testIfArtisanCommandSaveDataAndSendEmailToAdmin()
    {
        Artisan::call('objects:import', [
            'filename' => 'test.xlsx'
        ]);

        $objects = Object::all();

        $this->assertNotEmpty($objects);
        $this->assertCount($objects->count(), $objects);

        $this->assertEquals(0, count(Mail::failures()));
    }
}
