<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Import Info</title>
</head>
<body>
    <h1>Import info</h1>
    <p>Updated: {{ $info['updated'] }}, Created: {{$info['created']}}</p>
</body>
</html>