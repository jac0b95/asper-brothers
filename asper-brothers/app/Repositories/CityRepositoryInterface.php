<?php

namespace App\Repositories;

use App\City;

interface CityRepositoryInterface 
{

    /**
     * Get's all cities
     *
     * @return collection
     */
    public function getAll();

    /**
     * Create city with given name
     *
     * @return collection
     */

    public function create($properties);

    /**
     * Get's city with given name
     *
     * @return collection
     */
    public function getIfExists(string $name);

}