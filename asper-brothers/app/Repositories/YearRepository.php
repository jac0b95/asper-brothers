<?php

namespace App\Repositories;
use DB;

class YearRepository implements YearRepositoryInterface
{
    /**
    *
    * @param array
    * @return collection
    *
    */
    public function getByProperties(array $properties) {
        return $year = DB::table('cities')
                    ->select('years.*')
                    ->join('streets', 'cities.id', '=', 'streets.city_id')
                    ->join('years', 'streets.id', '=', 'years.street_id')
                    ->where('streets.city_id', $properties['city'])
                    ->where('years.street_id', $properties['street'])
                    ->get();
    }

    /**
    *
    * @param string
    * @param array
    * @return collection
    *
    */
    public function getIfExists(string $year, array $properties) {
        return $year = DB::table('cities')
                    ->select('years.*')
                    ->join('streets', 'cities.id', '=', 'streets.city_id')
                    ->join('years', 'streets.id', '=', 'years.street_id')
                    ->where('streets.city_id', $properties['city'])
                    ->where('years.street_id', $properties['street'])
                    ->where('years.year', $year)
                    ->get();
    }
}