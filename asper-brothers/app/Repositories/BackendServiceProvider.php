<?php
namespace App\Repositories;

use Illuminate\Support\ServiceProvider;

class BackendServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(
            'App\Repositories\ObjectRepositoryInterface',
            'App\Repositories\ObjectRepository'
        );
        
        $this->app->bind(
            'App\Repositories\CityRepositoryInterface',
            'App\Repositories\CityRepository'
        );
        
        $this->app->bind(
            'App\Repositories\StreetRepositoryInterface',
            'App\Repositories\StreetRepository'
        );
        
        $this->app->bind(
            'App\Repositories\YearRepositoryInterface',
            'App\Repositories\YearRepository'
        );
    }
}