<?php

namespace App\Repositories;

use App\City;
use App\Street;

class StreetRepository implements StreetRepositoryInterface
{
    /**
    *
    * @param int
    * @return collection
    *
    */
    public function getByProperties(int $cityID) {
        return Street::where('city_id', '=', $cityID)->get();
    }

    /**
    *
    * @param string
    * @param int
    * @return collection
    *
    */
    public function getIfExists(string $name, int $cityID) {
        return Street::where('city_id', '=', $cityID)->where('name', '=', $name)->get();
    }
}