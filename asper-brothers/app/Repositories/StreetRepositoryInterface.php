<?php

namespace App\Repositories;

interface StreetRepositoryInterface 
{
    /** 
    *
    * @param int
    * @return collection
    *
    */
    public function getByProperties(int $cityID);

    /**
    *
    * @param string
    * @param int
    * @return collection
    *
    */
    public function getIfExists(string $name, int $cityID);
}