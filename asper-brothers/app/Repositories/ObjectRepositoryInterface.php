<?php

namespace App\Repositories;

use App\Object;
use App\City;
use App\Street;
use App\Year;

interface ObjectRepositoryInterface 
{
    /**
     * Get's a object by properties
     *
     * @param array
     */
    public function getByProperties($properties);

    /**
    * Creates or update an object if exists 
    * @param array
    */
    public function createOrUpdate($properties);

    /**
    * Check if object exists by it properties (strings)
    * @param array
    */
    public function getIfExists(string $name, City $city, Street $street, Year $year);

}