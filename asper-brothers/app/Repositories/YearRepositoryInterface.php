<?php

namespace App\Repositories;

use App\Year;

interface YearRepositoryInterface 
{
/**
    *
    * @param array
    * @return collection
    *
    */
    public function getByProperties(array $properties);

    /**
    *
    * @param string
    * @param array
    * @return collection
    *
    */
    public function getIfExists(string $year, array $properties) ;
}