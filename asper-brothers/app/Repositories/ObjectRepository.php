<?php

namespace App\Repositories;

use App\Object;
use App\City;
use App\Street;
use App\Year;
use DB;

class ObjectRepository implements ObjectRepositoryInterface
{
    public $updated = false;
    public $created = false;
    /**
    * Creates or update an object if exists 
    * @param array
    */
    public function createOrUpdate($properties)
    {
        $object = $this->getByProperties($properties);
        if($object->isEmpty()) {
            $city = City::firstOrCreate(['name' => $properties['city']]);
            $street = Street::firstOrCreate(['name' => $properties['street'], 'city_id' => $city->id]);
            $year = Year::firstOrCreate(['year' => $properties['year'], 'street_id' => $street->id]);
            $object = Object::firstOrCreate(['name' => $properties['object'], 'year_id' => $year->id]);
            $this->created = true;
        }
        else {
            /* 
                If object is all the same do nothing and set update to true
            */
            $this->updated = true;
        }
    }
    /**
     * Get's a object by it's properties (id's)
     *
     * @param array
     * @return collection
     */
    public function getByProperties($properties)
    {
        $object = DB::table('cities')
                    ->select('objects.*')
                    ->join('streets', 'cities.id', '=', 'streets.city_id')
                    ->join('years', 'streets.id', '=', 'years.street_id')
                    ->join('objects', 'years.id', '=', 'objects.year_id')
                    ->where('cities.name', $properties['city'])
                    ->where('streets.name', $properties['street'])
                    ->where('years.year', $properties['year'])
                    ->get();

        return $object;
    }

    public function getByIDs($properties)
    {
        $object = DB::table('cities')
                    ->select('objects.*')
                    ->join('streets', 'cities.id', '=', 'streets.city_id')
                    ->join('years', 'streets.id', '=', 'years.street_id')
                    ->join('objects', 'years.id', '=', 'objects.year_id')
                    ->where('streets.city_id', $properties['city'])
                    ->where('years.street_id', $properties['street'])
                    ->where('objects.year_id', $properties['year'])
                    ->get();

        return $object;
    }

    /**
     * Get's a object by it's properties (strings)
     *
     * @param array
     * @return collection
     */

    public function getIfExists(string $name, City $city, Street $street, Year $year)
    {
        $object = DB::table('cities')
                    ->select('objects.*')
                    ->join('streets', 'cities.id', '=', 'streets.city_id')
                    ->join('years', 'streets.id', '=', 'years.street_id')
                    ->join('objects', 'years.id', '=', 'objects.year_id')
                    ->where('cities.id', $city->id)
                    ->where('streets.id', $street->id)
                    ->where('years.year', $year->id)
                    ->where('objects.name', '=', $name)
                    ->get();

        return $object;
    }

    public function wasUpdated() {
        return !$this->created && $this->updated;
    }

    public function wasCreated() {
        return $this->created && !$this->updated;
    }
}