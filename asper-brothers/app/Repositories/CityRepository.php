<?php

namespace App\Repositories;

use App\City;

class CityRepository implements CityRepositoryInterface
{
    /**
     * Get's all cities
     *
     * @return collection
     */
    public function getAll() {
        return City::all();
    }

    /**
     * Create city with given name
     *
     * @return collection
     */

    public function create($properties) {
        return City::create([
            'name' => $properties['name']
        ]);
    }

    /**
     * Get's city with given name
     *
     * @return collection
     */
    public function getIfExists(string $name) {
        return City::where('name', '=', $name)->first();
    }

}