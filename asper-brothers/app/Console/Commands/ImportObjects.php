<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Manager\Import;

class ImportObjects extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'objects:import {filename : Name of xlsx/xls file to import}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import objects to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Import $importManager)
    {

        $filename = $this->argument('filename');

        $importManager->import($filename);

        $this->info($importManager->importInfo());
    }
}
