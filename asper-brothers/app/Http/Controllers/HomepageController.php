<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CityRepository;

class HomepageController extends Controller
{
    public function index(CityRepository $repo) {
        
        $cities = $repo->getAll();
        
        return view('welcome')->with('cities', $cities);
    }
}
