<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\StreetRepository;
use App\Repositories\YearRepository;
use App\Repositories\ObjectRepository;

class TableDataController extends Controller
{
    public function getStreets(StreetRepository $repo, Request $request) {
        return response()->json($repo->getByProperties($request->city));
    }

    public function getYears(YearRepository $repo, Request $request) {
        $properties = array(
            'city' => $request->city,
            'street' => $request->street,
        );
        return response()->json($repo->getByProperties($properties));
    }

    public function getObjects(ObjectRepository $repo, Request $request) {
        $properties = array(
            'city' => $request->city,
            'street' => $request->street,
            'year' => $request->year,
        );
        return response()->json($repo->getByIDs($properties));
    }
}
