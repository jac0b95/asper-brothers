<?php

namespace App\Manager;

use App\Imports\ObjectsImport;
use Excel;
use App\City;
use App\Street;
use App\Year;
use App\Object;
use App\Repositories\ObjectRepository;
use App\Mail\Info;
use App\Imports\Importt;

class Import {
    
    protected $updatedCount = 0;
    protected $createdCount = 0;

    private $repo;

    public function __construct(ObjectRepository $repo) {
        $this->repo = $repo;
    }

    public function import(string $filename = 'test.xlsx')
    {
        $sheets = Excel::toArray(new ObjectsImport, public_path(config('custom.import_files_folder') . '/' . $filename));
        
        $rows = $sheets[0];

        foreach($rows as $row) 
        {
            $properties = array(
                'city' => $row['miasto'],
                'street' => $row['ulica'],
                'year' => $row['rok'],
                'object' => $row['nazwa_obiektu']
            );
            

            $this->repo->createOrUpdate($properties);
            
            if($this->repo->wasUpdated()) {
                $this->updatedCount++;
            }

            if($this->repo->wasCreated()) {
                $this->createdCount++;
            }
        }

        $this->sendEmailToAdmin();
    }

    private function sendEmailToAdmin() {
        
        $info = array(
            'updated' => $this->updatedCount, 
            'created' => $this->createdCount
        );

        \Mail::to(config('custom.admin_email'))->send(new Info($info));
    }

    public function importInfo() {
        return 'updated: '. $this->updatedCount . ' created: ' . $this->createdCount;
    }
}

